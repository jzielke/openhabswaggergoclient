// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ThingTypeUID thing type UID
// swagger:model ThingTypeUID
type ThingTypeUID struct {

	// as string
	AsString string `json:"asString,omitempty"`

	// binding Id
	BindingID string `json:"bindingId,omitempty"`

	// id
	ID string `json:"id,omitempty"`
}

// Validate validates this thing type UID
func (m *ThingTypeUID) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ThingTypeUID) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ThingTypeUID) UnmarshalBinary(b []byte) error {
	var res ThingTypeUID
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
