// Code generated by go-swagger; DO NOT EDIT.

package extensions

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new extensions API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for extensions API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
GetExtensionByID gets extension with given ID
*/
func (a *Client) GetExtensionByID(params *GetExtensionByIDParams) (*GetExtensionByIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetExtensionByIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getExtensionById",
		Method:             "GET",
		PathPattern:        "/extensions/{extensionId}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetExtensionByIDReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetExtensionByIDOK), nil

}

/*
GetExtensions gets all extensions
*/
func (a *Client) GetExtensions(params *GetExtensionsParams) (*GetExtensionsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetExtensionsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getExtensions",
		Method:             "GET",
		PathPattern:        "/extensions",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetExtensionsReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetExtensionsOK), nil

}

/*
GetTypes gets all extension types
*/
func (a *Client) GetTypes(params *GetTypesParams) (*GetTypesOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetTypesParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getTypes",
		Method:             "GET",
		PathPattern:        "/extensions/types",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetTypesReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetTypesOK), nil

}

/*
InstallExtension installs the extension with the given ID
*/
func (a *Client) InstallExtension(params *InstallExtensionParams) (*InstallExtensionOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewInstallExtensionParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "installExtension",
		Method:             "POST",
		PathPattern:        "/extensions/{extensionId}/install",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &InstallExtensionReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*InstallExtensionOK), nil

}

/*
InstallExtensionByURL installs the extension from the given URL
*/
func (a *Client) InstallExtensionByURL(params *InstallExtensionByURLParams) (*InstallExtensionByURLOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewInstallExtensionByURLParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "installExtensionByURL",
		Method:             "POST",
		PathPattern:        "/extensions/url/{url}/install",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &InstallExtensionByURLReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*InstallExtensionByURLOK), nil

}

/*
UninstallExtension uninstall extension API
*/
func (a *Client) UninstallExtension(params *UninstallExtensionParams) (*UninstallExtensionOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUninstallExtensionParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "uninstallExtension",
		Method:             "POST",
		PathPattern:        "/extensions/{extensionId}/uninstall",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UninstallExtensionReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*UninstallExtensionOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
