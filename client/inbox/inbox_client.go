// Code generated by go-swagger; DO NOT EDIT.

package inbox

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new inbox API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for inbox API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
Approve approves the discovery result by adding the thing to the registry
*/
func (a *Client) Approve(params *ApproveParams) (*ApproveOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewApproveParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "approve",
		Method:             "POST",
		PathPattern:        "/inbox/{thingUID}/approve",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{"text/plain"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &ApproveReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*ApproveOK), nil

}

/*
Delete removes the discovery result from the inbox
*/
func (a *Client) Delete(params *DeleteParams) (*DeleteOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "delete",
		Method:             "DELETE",
		PathPattern:        "/inbox/{thingUID}",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &DeleteReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*DeleteOK), nil

}

/*
GetAllDiscoveredThings gets all discovered things
*/
func (a *Client) GetAllDiscoveredThings(params *GetAllDiscoveredThingsParams) (*GetAllDiscoveredThingsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetAllDiscoveredThingsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getAllDiscoveredThings",
		Method:             "GET",
		PathPattern:        "/inbox",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetAllDiscoveredThingsReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetAllDiscoveredThingsOK), nil

}

/*
Ignore flags a discovery result as ignored for further processing
*/
func (a *Client) Ignore(params *IgnoreParams) (*IgnoreOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewIgnoreParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "ignore",
		Method:             "POST",
		PathPattern:        "/inbox/{thingUID}/ignore",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &IgnoreReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*IgnoreOK), nil

}

/*
Unignore removes ignore flag from a discovery result
*/
func (a *Client) Unignore(params *UnignoreParams) (*UnignoreOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUnignoreParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "unignore",
		Method:             "POST",
		PathPattern:        "/inbox/{thingUID}/unignore",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UnignoreReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*UnignoreOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
