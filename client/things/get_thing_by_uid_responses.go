// Code generated by go-swagger; DO NOT EDIT.

package things

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "gitlab.com/jzielke/openhabswaggergoclient/models"
)

// GetThingByUIDReader is a Reader for the GetThingByUID structure.
type GetThingByUIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetThingByUIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetThingByUIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 404:
		result := NewGetThingByUIDNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetThingByUIDOK creates a GetThingByUIDOK with default headers values
func NewGetThingByUIDOK() *GetThingByUIDOK {
	return &GetThingByUIDOK{}
}

/*GetThingByUIDOK handles this case with default header values.

OK
*/
type GetThingByUIDOK struct {
	Payload *models.ThingDTO
}

func (o *GetThingByUIDOK) Error() string {
	return fmt.Sprintf("[GET /things/{thingUID}][%d] getThingByUidOK  %+v", 200, o.Payload)
}

func (o *GetThingByUIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ThingDTO)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetThingByUIDNotFound creates a GetThingByUIDNotFound with default headers values
func NewGetThingByUIDNotFound() *GetThingByUIDNotFound {
	return &GetThingByUIDNotFound{}
}

/*GetThingByUIDNotFound handles this case with default header values.

Thing not found.
*/
type GetThingByUIDNotFound struct {
}

func (o *GetThingByUIDNotFound) Error() string {
	return fmt.Sprintf("[GET /things/{thingUID}][%d] getThingByUidNotFound ", 404)
}

func (o *GetThingByUIDNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
