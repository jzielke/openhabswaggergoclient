// Code generated by go-swagger; DO NOT EDIT.

package things

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "gitlab.com/jzielke/openhabswaggergoclient/models"
)

// UpdateThingConfigurationReader is a Reader for the UpdateThingConfiguration structure.
type UpdateThingConfigurationReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UpdateThingConfigurationReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewUpdateThingConfigurationOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 400:
		result := NewUpdateThingConfigurationBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewUpdateThingConfigurationNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 409:
		result := NewUpdateThingConfigurationConflict()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewUpdateThingConfigurationOK creates a UpdateThingConfigurationOK with default headers values
func NewUpdateThingConfigurationOK() *UpdateThingConfigurationOK {
	return &UpdateThingConfigurationOK{}
}

/*UpdateThingConfigurationOK handles this case with default header values.

OK
*/
type UpdateThingConfigurationOK struct {
	Payload *models.Thing
}

func (o *UpdateThingConfigurationOK) Error() string {
	return fmt.Sprintf("[PUT /things/{thingUID}/config][%d] updateThingConfigurationOK  %+v", 200, o.Payload)
}

func (o *UpdateThingConfigurationOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Thing)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewUpdateThingConfigurationBadRequest creates a UpdateThingConfigurationBadRequest with default headers values
func NewUpdateThingConfigurationBadRequest() *UpdateThingConfigurationBadRequest {
	return &UpdateThingConfigurationBadRequest{}
}

/*UpdateThingConfigurationBadRequest handles this case with default header values.

Configuration of the thing is not valid.
*/
type UpdateThingConfigurationBadRequest struct {
}

func (o *UpdateThingConfigurationBadRequest) Error() string {
	return fmt.Sprintf("[PUT /things/{thingUID}/config][%d] updateThingConfigurationBadRequest ", 400)
}

func (o *UpdateThingConfigurationBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewUpdateThingConfigurationNotFound creates a UpdateThingConfigurationNotFound with default headers values
func NewUpdateThingConfigurationNotFound() *UpdateThingConfigurationNotFound {
	return &UpdateThingConfigurationNotFound{}
}

/*UpdateThingConfigurationNotFound handles this case with default header values.

Thing not found
*/
type UpdateThingConfigurationNotFound struct {
}

func (o *UpdateThingConfigurationNotFound) Error() string {
	return fmt.Sprintf("[PUT /things/{thingUID}/config][%d] updateThingConfigurationNotFound ", 404)
}

func (o *UpdateThingConfigurationNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewUpdateThingConfigurationConflict creates a UpdateThingConfigurationConflict with default headers values
func NewUpdateThingConfigurationConflict() *UpdateThingConfigurationConflict {
	return &UpdateThingConfigurationConflict{}
}

/*UpdateThingConfigurationConflict handles this case with default header values.

Thing could not be updated as it is not editable.
*/
type UpdateThingConfigurationConflict struct {
}

func (o *UpdateThingConfigurationConflict) Error() string {
	return fmt.Sprintf("[PUT /things/{thingUID}/config][%d] updateThingConfigurationConflict ", 409)
}

func (o *UpdateThingConfigurationConflict) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
