// Code generated by go-swagger; DO NOT EDIT.

package things

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "gitlab.com/jzielke/openhabswaggergoclient/models"
)

// GetAllThingsReader is a Reader for the GetAllThings structure.
type GetAllThingsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetAllThingsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetAllThingsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetAllThingsOK creates a GetAllThingsOK with default headers values
func NewGetAllThingsOK() *GetAllThingsOK {
	return &GetAllThingsOK{}
}

/*GetAllThingsOK handles this case with default header values.

OK
*/
type GetAllThingsOK struct {
	Payload []*models.EnrichedThingDTO
}

func (o *GetAllThingsOK) Error() string {
	return fmt.Sprintf("[GET /things][%d] getAllThingsOK  %+v", 200, o.Payload)
}

func (o *GetAllThingsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
