// Code generated by go-swagger; DO NOT EDIT.

package things

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new things API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for things API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
Create creates a new thing and adds it to the registry
*/
func (a *Client) Create(params *CreateParams) (*CreateCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewCreateParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "create",
		Method:             "POST",
		PathPattern:        "/things",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &CreateReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*CreateCreated), nil

}

/*
GetAllThings gets all available things
*/
func (a *Client) GetAllThings(params *GetAllThingsParams) (*GetAllThingsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetAllThingsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getAllThings",
		Method:             "GET",
		PathPattern:        "/things",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetAllThingsReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetAllThingsOK), nil

}

/*
GetConfigStatus gets thing s config status
*/
func (a *Client) GetConfigStatus(params *GetConfigStatusParams) (*GetConfigStatusOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetConfigStatusParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getConfigStatus",
		Method:             "GET",
		PathPattern:        "/things/{thingUID}/config/status",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetConfigStatusReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetConfigStatusOK), nil

}

/*
GetFirmwareStatus gets thing s firmware status
*/
func (a *Client) GetFirmwareStatus(params *GetFirmwareStatusParams) (*GetFirmwareStatusOK, *GetFirmwareStatusNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetFirmwareStatusParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getFirmwareStatus",
		Method:             "GET",
		PathPattern:        "/things/{thingUID}/firmware/status",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetFirmwareStatusReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, nil, err
	}
	switch value := result.(type) {
	case *GetFirmwareStatusOK:
		return value, nil, nil
	case *GetFirmwareStatusNoContent:
		return nil, value, nil
	}
	return nil, nil, nil

}

/*
GetFirmwares gets all available firmwares for provided thing UID
*/
func (a *Client) GetFirmwares(params *GetFirmwaresParams) (*GetFirmwaresOK, *GetFirmwaresNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetFirmwaresParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getFirmwares",
		Method:             "GET",
		PathPattern:        "/things/{thingUID}/firmwares",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetFirmwaresReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, nil, err
	}
	switch value := result.(type) {
	case *GetFirmwaresOK:
		return value, nil, nil
	case *GetFirmwaresNoContent:
		return nil, value, nil
	}
	return nil, nil, nil

}

/*
GetStatus gets thing s status
*/
func (a *Client) GetStatus(params *GetStatusParams) (*GetStatusOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetStatusParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getStatus",
		Method:             "GET",
		PathPattern:        "/things/{thingUID}/status",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetStatusReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetStatusOK), nil

}

/*
GetThingByUID gets thing by UID
*/
func (a *Client) GetThingByUID(params *GetThingByUIDParams) (*GetThingByUIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetThingByUIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getThingByUID",
		Method:             "GET",
		PathPattern:        "/things/{thingUID}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetThingByUIDReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetThingByUIDOK), nil

}

/*
Remove removes a thing from the registry set force to true if you want the thing te be removed immediately
*/
func (a *Client) Remove(params *RemoveParams) (*RemoveOK, *RemoveAccepted, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewRemoveParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "remove",
		Method:             "DELETE",
		PathPattern:        "/things/{thingUID}",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &RemoveReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, nil, err
	}
	switch value := result.(type) {
	case *RemoveOK:
		return value, nil, nil
	case *RemoveAccepted:
		return nil, value, nil
	}
	return nil, nil, nil

}

/*
Update updates a thing
*/
func (a *Client) Update(params *UpdateParams) (*UpdateOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUpdateParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "update",
		Method:             "PUT",
		PathPattern:        "/things/{thingUID}",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UpdateReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*UpdateOK), nil

}

/*
UpdateFirmware updates thing firmware
*/
func (a *Client) UpdateFirmware(params *UpdateFirmwareParams) (*UpdateFirmwareOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUpdateFirmwareParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "updateFirmware",
		Method:             "PUT",
		PathPattern:        "/things/{thingUID}/firmware/{firmwareVersion}",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UpdateFirmwareReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*UpdateFirmwareOK), nil

}

/*
UpdateThingConfiguration updates thing s configuration
*/
func (a *Client) UpdateThingConfiguration(params *UpdateThingConfigurationParams) (*UpdateThingConfigurationOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUpdateThingConfigurationParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "updateThingConfiguration",
		Method:             "PUT",
		PathPattern:        "/things/{thingUID}/config",
		ProducesMediaTypes: []string{""},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UpdateThingConfigurationReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*UpdateThingConfigurationOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
