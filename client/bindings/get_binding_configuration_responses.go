// Code generated by go-swagger; DO NOT EDIT.

package bindings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// GetBindingConfigurationReader is a Reader for the GetBindingConfiguration structure.
type GetBindingConfigurationReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetBindingConfigurationReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetBindingConfigurationOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 404:
		result := NewGetBindingConfigurationNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 500:
		result := NewGetBindingConfigurationInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetBindingConfigurationOK creates a GetBindingConfigurationOK with default headers values
func NewGetBindingConfigurationOK() *GetBindingConfigurationOK {
	return &GetBindingConfigurationOK{}
}

/*GetBindingConfigurationOK handles this case with default header values.

OK
*/
type GetBindingConfigurationOK struct {
	Payload string
}

func (o *GetBindingConfigurationOK) Error() string {
	return fmt.Sprintf("[GET /bindings/{bindingId}/config][%d] getBindingConfigurationOK  %+v", 200, o.Payload)
}

func (o *GetBindingConfigurationOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetBindingConfigurationNotFound creates a GetBindingConfigurationNotFound with default headers values
func NewGetBindingConfigurationNotFound() *GetBindingConfigurationNotFound {
	return &GetBindingConfigurationNotFound{}
}

/*GetBindingConfigurationNotFound handles this case with default header values.

Binding does not exist
*/
type GetBindingConfigurationNotFound struct {
}

func (o *GetBindingConfigurationNotFound) Error() string {
	return fmt.Sprintf("[GET /bindings/{bindingId}/config][%d] getBindingConfigurationNotFound ", 404)
}

func (o *GetBindingConfigurationNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetBindingConfigurationInternalServerError creates a GetBindingConfigurationInternalServerError with default headers values
func NewGetBindingConfigurationInternalServerError() *GetBindingConfigurationInternalServerError {
	return &GetBindingConfigurationInternalServerError{}
}

/*GetBindingConfigurationInternalServerError handles this case with default header values.

Configuration can not be read due to internal error
*/
type GetBindingConfigurationInternalServerError struct {
}

func (o *GetBindingConfigurationInternalServerError) Error() string {
	return fmt.Sprintf("[GET /bindings/{bindingId}/config][%d] getBindingConfigurationInternalServerError ", 500)
}

func (o *GetBindingConfigurationInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
